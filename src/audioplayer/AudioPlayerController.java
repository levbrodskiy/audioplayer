package audioplayer;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AudioPlayerController {
    private List<File> playList;
    private AudioPlayer player;

    @FXML
    private Label trackNameLable;

    @FXML
    private Button playButton;

    @FXML
    private Button stopButton;

    @FXML
    private Button pauseButton;

    @FXML
    private Button backMusicButton;

    @FXML
    private Button nextMusicButton;

    @FXML
    private TextField pathTextField;

    @FXML
    private Button getPathButton;

    @FXML
    void clickGetPathButton(ActionEvent event) {
        String path = pathTextField.getText();
        try {
            File directory = new File(path);
            playList = new ArrayList();
            for (File file : directory.listFiles()) {
                if (file.isFile() && file.getName().contains(".mp3")){
                    playList.add(file.getAbsoluteFile());
                }
            }
        }catch (Exception e){
            showAlert("Files not found", "Please, enter the correct path");
            return;
        }
        if (!playList.isEmpty()){
            createPlayer();
            setDisableAllButtons(false);
        }else {
            showAlert("Files not found", "Files not found");
            return;
        }

    }

    @FXML
    void clickPauseButton(ActionEvent event) {
        player.pause();
    }

    @FXML
    void clickPlayButton(ActionEvent event) {
        player.play();
    }

    @FXML
    void clickStopButton(ActionEvent event) {
        player.stop();
    }

    private void createPlayer(){
        player = new AudioPlayer(playList);
        trackNameLable.setText(player.getTrackName());
    }

    @FXML
    void backMusicButtonClick(ActionEvent event) {
       player.back();
       trackNameLable.setText(player.getTrackName());
    }

    @FXML
    void nextMusicButtonClick(ActionEvent event) {
        player.next();
        trackNameLable.setText(player.getTrackName());
    }

    /**
     * Выводит на экран информационное сообщение с указанным заголовком и сообщением
     * @param title - заголовок сообщения
     * @param text - текст сообщения
     */
    private void showAlert(String title, String text){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setContentText(text);
        alert.showAndWait();
    }
    private void setDisableAllButtons(boolean disable){
        playButton.setDisable(disable);
        pauseButton.setDisable(disable);
        stopButton.setDisable(disable);
        nextMusicButton.setDisable(disable);
        backMusicButton.setDisable(disable);
    }

}
