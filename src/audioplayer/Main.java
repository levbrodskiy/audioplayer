package audioplayer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("audioPlayer.fxml"));
        primaryStage.setTitle("AudioPlayer");
        primaryStage.setScene(new Scene(root, 332, 221));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
