package audioplayer;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.util.List;

/**
 *Предоставляет элементы управления аудиофайлами на основе JavaFX MediaPlayer
 */
public class AudioPlayer {
    private MediaPlayer player;
    private List playList;
    private int trackCount = 0;
    private String trackName;

    /**
     * создаёт объект аудиоплеера с плейлистом
     * @param playList
     */
    public AudioPlayer(List playList){
        this.playList = playList;
        createTrack();
    }

    public String getTrackName(){
        return trackName;
    }

    /**
     * запускает проигрывание трека
     */
    public void play(){
        player.play();
    }
    /**
     * ставит на паузу проигрывание трека
     */
    public void pause(){
        player.pause();
    }
    /**
     * останавливает проигрывание трека
     */
    public void stop(){
        player.stop();
    }

    private void createTrack(){
        File track = new File
                (playList.get(trackCount).toString());
        player = new MediaPlayer(new Media(track.toURI().toString()));
        trackName = track.getName();
    }

    /**
     * переключает аудиоплеер на следующую песню в плейлисте
     */
    public void next(){
            if (playList.size()-1 > trackCount){
                trackName = playList.get(++trackCount).toString();
                createTrack();
            }
    }

    /**
     * переключает аудиоплеер на предыдущую песню в плейлисте
     */
    public void back(){
        if (trackCount > 0){
            trackName = playList.get(--trackCount).toString();
            createTrack();
        }
    }
}
